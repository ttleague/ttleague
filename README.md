Table Tennis League
===================

[![Dependency Status](https://www.versioneye.com/user/projects/56ec72fe05cd9700120ea782/badge.svg?style=flat)](https://www.versioneye.com/user/projects/56ec72fe05cd9700120ea782)
[![Run Status](https://api.shippable.com/projects/560ee5471895ca447419a292/badge?branch=master)](https://app.shippable.com/projects/560ee5471895ca447419a292)
[![Run Status](https://api.shippable.com/projects/560ee5471895ca447419a292/coverageBadge?branch=master)](https://app.shippable.com/projects/560ee5471895ca447419a292)

This project is a playground for Domain Driven Design techniques.