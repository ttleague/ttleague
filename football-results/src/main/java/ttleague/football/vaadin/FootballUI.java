package ttleague.football.vaadin;

import com.vaadin.annotations.Push;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringUI
@Push(PushMode.AUTOMATIC)
public class FootballUI extends UI {

	@Override
	protected void init(VaadinRequest request) {
		TabSheet tabSheet = new TabSheet();
		tabSheet.addTab(new Label("<h2>Add Match</h2>", ContentMode.HTML), "Add Match");
		tabSheet.addTab(new Label("<h2>History</h2>", ContentMode.HTML), "History");
		tabSheet.addTab(new Label("<h2>Ranking</h2>", ContentMode.HTML), "Ranking");
		tabSheet.addTab(new Label("<h2>Admin</h2>", ContentMode.HTML), "Admin");

		setContent(new VerticalLayout(new Label("<h1>Football Matches</h1>", ContentMode.HTML), tabSheet));
	}
}
