package ttleague.player.app;

import org.axonframework.common.jdbc.ConnectionProvider;
import org.axonframework.common.jdbc.UnitOfWorkAwareConnectionProviderWrapper;
import org.axonframework.common.transaction.TransactionManager;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine;
import org.axonframework.eventsourcing.eventstore.jdbc.EventTableFactory;
import org.axonframework.eventsourcing.eventstore.jdbc.HsqlEventTableFactory;
import org.axonframework.eventsourcing.eventstore.jdbc.JdbcEventStorageEngine;
import org.axonframework.spring.config.AnnotationDriven;
import org.axonframework.spring.config.EnableAxon;
import org.axonframework.spring.config.TransactionManagerFactoryBean;
import org.axonframework.spring.jdbc.SpringDataSourceConnectionProvider;
import org.axonframework.spring.messaging.unitofwork.SpringTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import ttleague.player.command.Player;

import javax.sql.DataSource;
import java.util.concurrent.atomic.*;

@Configuration
@EnableAxon
@AnnotationDriven
public class AxonConfiguration {

	@Bean
	public EventStorageEngine eventStorageEngine(@Autowired ConnectionProvider connectionProvider,
			@Autowired TransactionManager transactionManager, @Autowired EventTableFactory tableFactory) {
		JdbcEventStorageEngine storageEngine = new JdbcEventStorageEngine(connectionProvider, transactionManager);
		storageEngine.createSchema(tableFactory);
		return storageEngine;
		// return new InMemoryEventStorageEngine();
	}

	@Bean
	public ConnectionProvider connectionProvider(@Autowired DataSource dataSource) {
		return new UnitOfWorkAwareConnectionProviderWrapper(new SpringDataSourceConnectionProvider(dataSource));
	}

	@Bean
	public TransactionManagerFactoryBean transactionManagerFactoryBean(@Autowired PlatformTransactionManager txMgr) {
		TransactionManagerFactoryBean factory = new TransactionManagerFactoryBean();
		factory.setTransactionManager(txMgr);
		return factory;
	}

	@Bean
	@ConditionalOnProperty(name = "spring.datasource.platform", havingValue = "hsqldb")
	public EventTableFactory eventTableFactoryHsqldb() {
		return HsqlEventTableFactory.INSTANCE;
	}

}
