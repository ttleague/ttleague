package ttleague.player.command;

import lombok.Value;

@Value
public class CreatePlayer {

	private String pid;
	
	private String firstName;
	
	private String lastName;
	
	private Integer yearOfBirth;

	private boolean active;
}
