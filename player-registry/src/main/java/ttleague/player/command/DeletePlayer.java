package ttleague.player.command;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

import lombok.Value;

@Value
public class DeletePlayer {
	@TargetAggregateIdentifier
	private String pid;
}
