package ttleague.player.command;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

import lombok.Value;

@Value
public class ModifyPlayer {

	@TargetAggregateIdentifier
	private String pid;
	
	private String firstName;
	
	private String lastName;

	private Integer yearOfBirth;

	private boolean active;
}
