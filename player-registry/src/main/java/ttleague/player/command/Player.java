package ttleague.player.command;

import lombok.Getter;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;
import static org.axonframework.commandhandling.model.AggregateLifecycle.markDeleted;

@Getter
@Slf4j
@Aggregate
public class Player {

	private static final long serialVersionUID = 2350036211793964632L;

	@AggregateIdentifier
	private String pid;
	
	private String firstName;
	
	private String lastName;

	private Integer yearOfBirth;

	private boolean active;
	
	public Player() {
	}

	@CommandHandler
	public Player(CreatePlayer cmd) {
		apply(new PlayerCreated(cmd.getPid(), cmd.getFirstName(), cmd.getLastName(), cmd.getYearOfBirth(),
				cmd.isActive()));
	}
	
	@EventSourcingHandler
	public void on(PlayerCreated event) {
		this.pid = event.getPid();
		this.firstName = event.getFirstName();
		this.lastName = event.getLastName();
		this.yearOfBirth = event.getYearOfBirth();
		this.active = event.isActive();
	}
	
	@CommandHandler
	public void modifyPlayer(ModifyPlayer cmd) {
		apply(new PlayerModified(cmd.getPid(), cmd.getFirstName(), cmd.getLastName(), cmd.getYearOfBirth(),
				cmd.isActive()));
	}

	@EventHandler
	public void on(PlayerModified event) {
		this.pid = event.getPid();
		this.firstName = event.getFirstName();
		this.lastName = event.getLastName();
		this.active = event.isActive();
	}
	
	@CommandHandler
	private void deletePlayer(DeletePlayer cmd) {
		apply(new PlayerDeleted(cmd.getPid()));
	}
	
	@EventHandler
	private void on(PlayerDeleted event) {
		log.debug("On PLayerDeleted");
		this.pid = event.getPid();
		markDeleted();
	}
}
