package ttleague.player.command;

import lombok.Value;

@Value
public class PlayerCreated {

	private String pid;
	
	private String firstName;
	
	private String lastName;
	
	private Integer yearOfBirth;

	private boolean active;
}
