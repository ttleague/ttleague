package ttleague.player.command;

import lombok.Value;

@Value
public class PlayerDeleted {

	private String pid;
}
