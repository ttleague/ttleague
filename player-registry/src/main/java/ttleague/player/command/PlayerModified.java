package ttleague.player.command;

import lombok.Value;

@Value
public class PlayerModified {

	String pid;
	
	String firstName;
	
	String lastName;

	Integer yearOfBirth;

	boolean active;
}
