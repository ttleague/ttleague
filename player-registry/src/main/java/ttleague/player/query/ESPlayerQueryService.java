package ttleague.player.query;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

import javax.annotation.Resource;

import org.axonframework.eventhandling.EventHandler;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ttleague.player.command.PlayerCreated;
import ttleague.player.command.PlayerDeleted;
import ttleague.player.command.PlayerModified;

@Service
public class ESPlayerQueryService implements PlayerQueryService {

	@Resource
	private PlayerEntryESRepository playerEntryRepository;

	@Override
	public void addToQueryRepository(PlayerEntry player) {
		playerEntryRepository.save(player);
	}

	@Override
	public void deleteFromQueryRepository(PlayerEntry player) {
		playerEntryRepository.delete(player);
	}

	@Override
	public Page<PlayerEntry> findAll(Pageable pageable) {
		return playerEntryRepository.findAll(pageable);
	}

	public long count() {
		return playerEntryRepository.count();
	}

	@EventHandler
	public void on(PlayerCreated event) {
		PlayerEntry player = new PlayerEntry(event.getPid(), event.getFirstName(), event.getLastName(),
				event.getYearOfBirth(), true);
		addToQueryRepository(player);
	}

	@EventHandler
	public void on(PlayerModified event) {
		PlayerEntry player = getPlayer(event.getPid());
		player.setFirstName(event.getFirstName());
		player.setLastName(event.getLastName());
		player.setYearOfBirth(event.getYearOfBirth());
		player.setActive(event.isActive());
		playerEntryRepository.save(player);
	}

	@EventHandler
	public void on(PlayerDeleted event) {
		playerEntryRepository.delete(event.getPid());
	}

	@Override
	public PlayerEntry getPlayer(String playerId) {
		return playerEntryRepository.findOne(playerId);
	}
}
