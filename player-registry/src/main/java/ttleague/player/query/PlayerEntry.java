package ttleague.player.query;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

@Document(indexName = "player")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PlayerEntry implements Serializable {

	@Id
	private String pid;

    private String firstName;

    private String lastName;

    private int yearOfBirth;

    private boolean active;

    @java.beans.ConstructorProperties({"pid", "firstName", "lastName", "yearOfBirth", "active"})
    public PlayerEntry(String pid, String firstName, String lastName, int yearOfBirth, boolean active) {
        this.pid = pid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
        this.active = active;
    }

    public PlayerEntry() {
    }

    public String getPid() {
        return this.pid;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public int getYearOfBirth() {
        return this.yearOfBirth;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof PlayerEntry)) return false;
        final PlayerEntry other = (PlayerEntry) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$pid = this.getPid();
        final Object other$pid = other.getPid();
        if (this$pid == null ? other$pid != null : !this$pid.equals(other$pid)) return false;
        final Object this$firstName = this.getFirstName();
        final Object other$firstName = other.getFirstName();
        if (this$firstName == null ? other$firstName != null : !this$firstName.equals(other$firstName)) return false;
        final Object this$lastName = this.getLastName();
        final Object other$lastName = other.getLastName();
        if (this$lastName == null ? other$lastName != null : !this$lastName.equals(other$lastName)) return false;
        if (this.getYearOfBirth() != other.getYearOfBirth()) return false;
        if (this.isActive() != other.isActive()) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $pid = this.getPid();
        result = result * PRIME + ($pid == null ? 43 : $pid.hashCode());
        final Object $firstName = this.getFirstName();
        result = result * PRIME + ($firstName == null ? 43 : $firstName.hashCode());
        final Object $lastName = this.getLastName();
        result = result * PRIME + ($lastName == null ? 43 : $lastName.hashCode());
        result = result * PRIME + this.getYearOfBirth();
        result = result * PRIME + (this.isActive() ? 79 : 97);
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof PlayerEntry;
    }

    public String toString() {
        return "PlayerEntry(pid=" + this.getPid() + ", firstName=" + this.getFirstName() + ", lastName=" + this.getLastName() + ", yearOfBirth=" + this.getYearOfBirth() + ", active=" + this.isActive() + ")";
    }
}
