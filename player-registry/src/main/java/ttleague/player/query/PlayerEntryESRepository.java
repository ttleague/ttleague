package ttleague.player.query;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

public interface PlayerEntryESRepository extends ElasticsearchCrudRepository<PlayerEntry, String> {

}
