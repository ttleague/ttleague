package ttleague.player.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PlayerQueryService {
	void addToQueryRepository(PlayerEntry player);

	void deleteFromQueryRepository(PlayerEntry player);

	Page<PlayerEntry> findAll(Pageable pageable);

	PlayerEntry getPlayer(String playerId);

	long count();
}
