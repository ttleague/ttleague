package ttleague.player.rest;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.common.DefaultIdentifierFactory;
import org.axonframework.common.IdentifierFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ttleague.player.command.CreatePlayer;
import ttleague.player.command.DeletePlayer;
import ttleague.player.query.PlayerEntry;
import ttleague.player.query.PlayerQueryService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@Slf4j
public class PlayerController {

	private final IdentifierFactory identifierFactory = new DefaultIdentifierFactory();

	@Autowired
	private CommandGateway commandGateway;
	
	@Autowired
	private PlayerQueryService playerQueryService;
	
	public PlayerController() {
		log.debug("Initialize " + getClass());
	}

	@RequestMapping(value="/api/player", method = RequestMethod.GET)
	@ResponseBody
	public List<PlayerEntry> findAll(Principal principal, @RequestParam(required = false, defaultValue = "true") boolean activeOnly, Pageable pageable) {
		return playerQueryService.findAll(pageable).getContent();
	}
	
	@RequestMapping(value="/api/player/{playerId}", method = RequestMethod.GET)
	public PlayerEntry getPlayer(Principal principal, @PathVariable("playerId") String playerId) {
		return playerQueryService.getPlayer(playerId);
	}
	
	@RequestMapping(value = "/api/player", method = RequestMethod.POST)
	@ResponseBody
	public CreatePlayer createPlayer(Principal principal, @RequestBody @Valid CreatePlayer request) {

		CreatePlayer command = new CreatePlayer(identifierFactory.generateIdentifier(), request.getFirstName(),
				request.getLastName(), request.getYearOfBirth(), true);
		commandGateway.send(command);

		return command;
	}
	
	@RequestMapping(value ="/api/player/{playerId}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deletePlayer(Principal principal, @PathVariable("playerId") String playerId) {
		commandGateway.send(new DeletePlayer(playerId));
	}
}
