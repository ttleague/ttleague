package ttleague.player.vaadin;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;

import lombok.extern.slf4j.Slf4j;
import ttleague.player.query.PlayerEntry;
import ttleague.player.query.PlayerQueryService;

@Service
@Slf4j
public class ESPlayerDataProvider extends AbstractBackEndDataProvider<PlayerEntry, Void> {

	@Autowired
	private transient PlayerQueryService queryService;

	@Override
	protected Stream<PlayerEntry> fetchFromBackEnd(Query<PlayerEntry, Void> query) {
		Page<PlayerEntry> page = queryService.findAll(new PageRequest(query.getOffset(), query.getLimit()));
		return page.getContent().stream();
	}

	@Override
	protected int sizeInBackEnd(Query<PlayerEntry, Void> query) {
		long count = queryService.count();
		if (count > Integer.MAX_VALUE) {
			throw new IllegalArgumentException("Value too large: " + count);
		}
		return Math.min(query.getLimit() - query.getOffset(), (int) count);
	}
}
