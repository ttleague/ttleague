package ttleague.player.vaadin;

import lombok.EqualsAndHashCode;
import org.axonframework.commandhandling.CommandCallback;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Push;
import com.vaadin.data.Binder;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.EditorSaveEvent;

import lombok.extern.slf4j.Slf4j;
import ttleague.player.command.ModifyPlayer;
import ttleague.player.query.PlayerEntry;

import java.io.PrintWriter;
import java.io.StringWriter;

@Slf4j
@SpringUI
@Push(PushMode.AUTOMATIC)
@EqualsAndHashCode
public class PlayerRegistryUI extends UI {

	private static final long serialVersionUID = 1L;

	private final Grid<PlayerEntry> table = new Grid<>();

	@Autowired
	private transient ESPlayerDataProvider dataprovider;

	@Autowired
	private transient CommandGateway commandGateway;

    @Override
	protected void init(VaadinRequest request) {

		table.addColumn(PlayerEntry::getFirstName).setCaption("First Name");
		table.addColumn(PlayerEntry::getLastName).setCaption("Last Name");

		Binder<PlayerEntry> playerEntryBinder = table.getEditor().getBinder();

		CheckBox activeField = new CheckBox();
		Binder.Binding<PlayerEntry, Boolean> activeBinding = playerEntryBinder.bind(activeField, PlayerEntry::isActive,
				PlayerEntry::setActive);
		Grid.Column<PlayerEntry, String> activeColumn = table.addColumn(entry -> String.valueOf(entry.isActive()))
				.setCaption("Active");
		activeColumn.setEditorBinding(activeBinding);
		table.getEditor().setEnabled(true);
		table.setHeightByRows(10.0);
		table.setDataProvider(dataprovider);
		table.getEditor().addSaveListener(this::onEditorSave);

		VerticalLayout mainContent = new VerticalLayout();
		mainContent.setSizeFull();
		setContent(mainContent);
		mainContent.addComponent(new Label("<h1>Players</h1>", ContentMode.HTML));
		mainContent.addComponent(table);

		table.setSizeFull();
		mainContent.setExpandRatio(table, 1);
    }

	private void onEditorSave(EditorSaveEvent<PlayerEntry> event) {
		PlayerEntry entry = event.getBean();
		commandGateway.send(new ModifyPlayer(entry.getPid(), entry.getFirstName(), entry.getLastName(),
				entry.getYearOfBirth(), entry.isActive()), new CommandCallback<ModifyPlayer, Object>() {
					@Override
					public void onSuccess(CommandMessage<? extends ModifyPlayer> commandMessage, Object result) {
						reloadTable();
					}

					@Override
					public void onFailure(CommandMessage<? extends ModifyPlayer> commandMessage, Throwable cause) {
						StringWriter sw = new StringWriter();
						cause.printStackTrace(new PrintWriter(sw));
						Notification.show(cause.getLocalizedMessage(), sw.toString(),
								Notification.Type.WARNING_MESSAGE);
					}
				});
	}

	private void reloadTable() {
		access(() -> table.setDataProvider(dataprovider));
	}
}
