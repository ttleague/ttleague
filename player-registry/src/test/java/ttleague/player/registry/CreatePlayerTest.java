package ttleague.player.registry;

import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;

import ttleague.player.command.CreatePlayer;
import ttleague.player.command.Player;
import ttleague.player.command.PlayerCreated;

public class CreatePlayerTest {
	
	private FixtureConfiguration<Player> fixture;
	
	@Before
	public void initFixture() {
		fixture = new AggregateTestFixture<>(Player.class);
	}
	
	@Test
	public void createNewPlayer() {
		fixture.givenNoPriorActivity()
				.when(new CreatePlayer("1", "Finn", "Huck", 1884, true))
				.expectEvents(new PlayerCreated("1", "Finn", "Huck", 1884, true));
	}
}
