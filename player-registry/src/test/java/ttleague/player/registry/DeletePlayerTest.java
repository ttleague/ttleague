package ttleague.player.registry;

import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;
import ttleague.player.command.*;

public class DeletePlayerTest {
	private FixtureConfiguration<Player> fixture;

	@Before
	public void initFixture() {
		fixture = new AggregateTestFixture<>(Player.class);
	}

	@Test
	public void deletePlayer() {
		fixture.given(new PlayerCreated("1", "Huck", "Finn", 1884, true)).when(new DeletePlayer("1"))
				.expectEvents(new PlayerDeleted("1")).expectSuccessfulHandlerExecution();
	}
}
