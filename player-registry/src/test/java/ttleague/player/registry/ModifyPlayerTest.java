package ttleague.player.registry;

import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;

import ttleague.player.command.ModifyPlayer;
import ttleague.player.command.Player;
import ttleague.player.command.PlayerCreated;
import ttleague.player.command.PlayerModified;

public class ModifyPlayerTest {
	private FixtureConfiguration<Player> fixture;
	
	@Before
	public void initFixture() {
		fixture = new AggregateTestFixture<>(Player.class);
	}
	
	@Test
	public void modifyPlayerTest() {
		fixture.given(new PlayerCreated("1", "Huck", "Finn", 1884, true))
				.when(new ModifyPlayer("1", "Tom", "Sawyer", 1884, true))
				.expectEvents(new PlayerModified("1", "Tom", "Sawyer", 1884, true));
	}
}
